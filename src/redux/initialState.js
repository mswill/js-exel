'use strict';

import { defaultStyles, defaultTitle } from "@/constants"

const defaultState = {
    title: defaultTitle,
    rowState: {},
    colState: {},
    dataState: {}, // {'0:1' : 'какой-то текст' }
    stylesState: {},
    currentText: '',
    currentStyles: defaultStyles,
    openedDate: new Date().toJSON()
}
const normalize = state => ({
    ...state,
    currentStyles: defaultStyles,
    currentText: ''
})

export function normalizeInitialState( state ) {
    return state ? normalize( state ) : defaultState
}
