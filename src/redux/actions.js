'use strict';

import { APPLY_STYLE, CHANGE_STYLES, CHANGE_TEXT, CHANGE_TITLE, TABLE_RESIZE, UPDATA_DATE } from "@/redux/types"

// Action creators
export function tableResize( data ) {
    return {
        type: TABLE_RESIZE,
        data
    }
}

export function changeText( data ) {
    return {
        type: CHANGE_TEXT,
        data
    }
}

export function changeStyles( data ) {
    return {
        type: CHANGE_STYLES,
        data
    }
}

// value, id's
export function applyStyle( data ) {
    return {
        type: APPLY_STYLE,
        data
    }
}


export function changeTitle( data ) {
    return {
        type: CHANGE_TITLE,
        data
    }
}

export function updateDate() {
    return {
        type: UPDATA_DATE,
    }
}
