'use strict';

export class Emitter {
    constructor() {
        this.listeners = {}
    }

    // dispatch, fire, trigger, emit
    emit( event, ...args ) {
        if ( !Array.isArray( this.listeners[ event ] ) ) {
            return false
        }
        this.listeners[ event ].forEach( listener => {
            listener( ...args )
        } )
        return true
    }


    // on, listen
    subscribe( event, fn ) {
        // если данное событие есть (которое передили), переприсвоить его. Иначе сделать новый массив
        this.listeners[ event ] = this.listeners[ event ] || []
        this.listeners[ event ].push( fn )

        // функция которая может отписаться от события
        return () => {
            this.listeners[ event ] =
                this.listeners[ event ].filter( listener => listener !== fn )
        }
    }
}
