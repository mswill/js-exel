'use strict';

export function createStore( rootReducer, initialState = {} ) {
    // разорачиаем (копируем обьект initialState (чтобы не было мутаций) и ставим начальный type __INIT__ как заглушку
    let state = rootReducer( { ...initialState }, { type: '__INIT__' } )
    let listeners = []
    return {
        subscribe( fn ) {
            listeners.push( fn )
            return {
                unsubscribe() {
                    listeners = listeners.filter( l => l !== fn )
                }
            }
        },
        dispatch( action ) {
            state = rootReducer( state, action )
            listeners.forEach( l => l( state ) )
        },
        getState() {
            return JSON.parse(JSON.stringify(state))
        }
    }
}
