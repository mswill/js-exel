'use strict';
import { DomListener } from '@core/DomListener'

export class ExcelComponent extends DomListener {
    constructor( $root, options = {} ) {
        super( $root, options.listeners )
        this.name = options.name || ''
        this.emitter = options.emitter
        this.subscribe = options.subscribe || []
        this.store = options.store
        this.unsubscribers = []
        this.prepare()
    }

    // Включается в компоненте до метода init
    prepare() {

    }

    // return template of component
    toHTML() {
        return ''
    }

    // Создаем метов по аналогии как во Vue
    // $emit служебный метод в фреймворке
    // уведомляем слушателей о событии event
    $emit( event, ...args ) {
        this.emitter.emit( event, ...args )
    }

    // подписываемся на event
    $on( event, fn ) {
        const unsub = this.emitter.subscribe( event, fn )
        this.unsubscribers.push( unsub )
    }

    // инициализируем компонент
    // добавляем DOM слушателей
    init() {
        this.initDOMListeners()
    }


    // удаляем компонент
    // чистим слушателей
    destroy() {
        this.removeDOMListeners()
        this.unsubscribers.forEach( unsub => unsub() )
    }

    $dispatch( action ) {
        this.store.dispatch( action )
    }

    // Здесь будуте те изменения по тем полям, на которые мы подписались
    storeChanged() {}

    isWatching(key) {
        return this.subscribe.includes( key )
    }
}

