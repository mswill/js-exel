import './sass/index.scss'
import { Router } from "@core/routes/Routes"
import { DashboardPage } from "@/pages/DashboardPage"
import { ExcelPage } from "@/pages/ExcelPage"


new Router( '#app', {
    dashboard: DashboardPage,
    excel: ExcelPage
} )
